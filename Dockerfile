FROM golang:1.13 as builder

ARG MAKE_TARGET="test build.bin"

WORKDIR "/code"
ADD . "/code"
RUN make BINARY=scaffold ${MAKE_TARGET}

FROM scratch
COPY --from=builder /code/scaffold /scaffold
ENTRYPOINT ["/scaffold"]
