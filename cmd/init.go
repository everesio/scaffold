package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/everesio/scaffold/pkg/config"
	"gitlab.com/everesio/scaffold/pkg/templ/init"
	"os"
)

func newInit() *cobra.Command {
	cfg := config.NewInitConfig()

	cmd := &cobra.Command{
		Use:   "init",
		Short: "Initial scaffolding",
		PreRunE: func(cmd *cobra.Command, args []string) error {
			cfg.SetDefaults()
			err := cfg.Validate()
			if err != nil {
				return err
			}
			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
			action := initaction.NewAction(cfg)
			err := action.Run()
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		},
	}
	cmd.Flags().StringVar(&cfg.Group, "group", "com.example", "the id of the project's group")
	cmd.Flags().StringVar(&cfg.Artifact, "artifact", "demo", "the id of the artifact")
	cmd.Flags().StringVar(&cfg.BaseDir, "base-dir", ".", "directory where the scaffold will be generated")

	return cmd
}

func init() {
	rootCmd.AddCommand(newInit())
}
