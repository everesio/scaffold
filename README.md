# scaffold - golang playground
Basic spring boot initializer developed in golang.


## Build 
* binary with [packr](https://github.com/gobuffalo/packr)  

```
    make clean build.bin 
```    

* docker image  

```
   make docker.build 
```    

## Help output

```
➜  scaffold git:(master) ✗ ./scaffold  
scaffold simple application

Usage:
  scaffold [command]

Available Commands:
  help        Help about any command
  init        Initial scaffolding

Flags:
      --config string   config file (default is $HOME/.scaffold.yaml)
  -h, --help            help for scaffold
  -t, --toggle          Help message for toggle

Use "scaffold [command] --help" for more information about a command.

➜  scaffold git:(master) ✗ ./scaffold init --help
Initial scaffolding

Usage:
  scaffold init [flags]

Flags:
      --artifact string   the id of the artifact (default "demo")
      --base-dir string   directory where the scaffold will be generated (default ".")
      --group string      the id of the project's group (default "com.example")
  -h, --help              help for init

Global Flags:
      --config string   config file (default is $HOME/.scaffold.yaml)

``` 

## Test

```
➜  scaffold git:(master) ✗ ./scaffold init
Create file ./demo/pom.xml
Create file ./demo/src/main/resources/application.properties
Create file ./demo/src/main/java/com/example/demo/DemoApplication.java
Create file ./demo/src/test/java/com/example/demo/DemoApplicationTests.java
Create file ./demo/Dockerfile
Create file ./demo/.gitignore

➜  scaffold git:(master) ✗ cd demo 

➜  demo git:(master) ✗ mvn spring-boot:run
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building Demo 0.0.1-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] >>> spring-boot-maven-plugin:2.1.8.RELEASE:run (default-cli) > test-compile @ demo >>>
[INFO] 
[INFO] --- maven-resources-plugin:3.1.0:resources (default-resources) @ demo ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 1 resource
[INFO] Copying 0 resource
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.1:compile (default-compile) @ demo ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] --- maven-resources-plugin:3.1.0:testResources (default-testResources) @ demo ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /home/michal/go/src/gitlab.com/everesio/scaffold/demo/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.1:testCompile (default-testCompile) @ demo ---
[INFO] Nothing to compile - all classes are up to date
[INFO] 
[INFO] <<< spring-boot-maven-plugin:2.1.8.RELEASE:run (default-cli) < test-compile @ demo <<<
[INFO] 
[INFO] --- spring-boot-maven-plugin:2.1.8.RELEASE:run (default-cli) @ demo ---

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.1.8.RELEASE)

2019-09-26 13:13:16.895  INFO 12268 --- [           main] com.example.demo.DemoApplication         : Starting DemoApplication on michal-extreme with PID 12268 (/home/michal/go/src/gitlab.com/everesio/scaffold/demo/target/classes started by michal in /home/michal/go/src/gitlab.com/everesio/scaffold/demo)
2019-09-26 13:13:16.897  INFO 12268 --- [           main] com.example.demo.DemoApplication         : No active profile set, falling back to default profiles: default
2019-09-26 13:13:17.236  INFO 12268 --- [           main] com.example.demo.DemoApplication         : Started DemoApplication in 0.534 seconds (JVM running for 2.258)
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 1.628 s
[INFO] Finished at: 2019-09-26T13:13:17+02:00
[INFO] Final Memory: 33M/593M
[INFO] ------------------------------------------------------------------------

```


