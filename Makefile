.DEFAULT_GOAL := build.bin

.PHONY: check clean build fmt test coverage

LDFLAGS       ?= -w -s
BUILD_FLAGS   ?=
BINARY        ?= scaffold
IMAGE         ?= scaffold

ROOT_DIR      := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

default: build

check:
	GOPRIVATE=gitlab.com
	go vet ./...
	golint $$(go list ./...) 2>&1

test:
	GOPRIVATE=gitlab.com GO111MODULE=on go test -mod=vendor -v ./...

coverage:
	GOPRIVATE=gitlab.com GO111MODULE=on go test -mod=vendor -v ./... -cover

build.bin:
	GOPRIVATE=gitlab.com
	GO111MODULE=on go get -u github.com/gobuffalo/packr/packr
	CGO_ENABLED=0 GO111MODULE=on packr build -mod=vendor -o $(BINARY) $(BUILD_FLAGS) -ldflags "$(LDFLAGS)" .

build:
	GOPRIVATE=gitlab.com CGO_ENABLED=0 GO111MODULE=on go build -mod=vendor -o $(BINARY) $(BUILD_FLAGS) -ldflags "$(LDFLAGS)" .

fmt:
	go fmt ./...

clean:
	@rm -rf $(BINARY)

.PHONY: deps
deps:
	GOPRIVATE=gitlab.com GO111MODULE=on go get ./...

.PHONY: vendor
vendor:
	GOPRIVATE=gitlab.com GO111MODULE=on go mod vendor

.PHONY: tidy
tidy:
	GOPRIVATE=gitlab.com GO111MODULE=on go mod tidy

.PHONY: docker.build
docker.build:
	docker build --pull -t $(IMAGE) -f Dockerfile .