# github.com/fsnotify/fsnotify v1.4.7
github.com/fsnotify/fsnotify
# github.com/go-playground/locales v0.12.1
github.com/go-playground/locales
github.com/go-playground/locales/currency
# github.com/go-playground/universal-translator v0.16.0
github.com/go-playground/universal-translator
# github.com/gobuffalo/envy v1.7.1
github.com/gobuffalo/envy
# github.com/gobuffalo/packd v0.3.0
github.com/gobuffalo/packd
github.com/gobuffalo/packd/internal/takeon/github.com/markbates/errx
# github.com/gobuffalo/packr v1.30.1
github.com/gobuffalo/packr
# github.com/hashicorp/hcl v1.0.0
github.com/hashicorp/hcl
github.com/hashicorp/hcl/hcl/ast
github.com/hashicorp/hcl/hcl/parser
github.com/hashicorp/hcl/hcl/printer
github.com/hashicorp/hcl/hcl/scanner
github.com/hashicorp/hcl/hcl/strconv
github.com/hashicorp/hcl/hcl/token
github.com/hashicorp/hcl/json/parser
github.com/hashicorp/hcl/json/scanner
github.com/hashicorp/hcl/json/token
# github.com/iancoleman/strcase v0.0.0-20190422225806-e506e3ef7365
github.com/iancoleman/strcase
# github.com/inconshreveable/mousetrap v1.0.0
github.com/inconshreveable/mousetrap
# github.com/joho/godotenv v1.3.0
github.com/joho/godotenv
# github.com/leodido/go-urn v1.1.0
github.com/leodido/go-urn
# github.com/magiconair/properties v1.8.0
github.com/magiconair/properties
# github.com/mitchellh/go-homedir v1.1.0
github.com/mitchellh/go-homedir
# github.com/mitchellh/mapstructure v1.1.2
github.com/mitchellh/mapstructure
# github.com/pelletier/go-toml v1.2.0
github.com/pelletier/go-toml
# github.com/pkg/errors v0.8.0
github.com/pkg/errors
# github.com/rogpeppe/go-internal v1.4.0
github.com/rogpeppe/go-internal/modfile
github.com/rogpeppe/go-internal/module
github.com/rogpeppe/go-internal/semver
# github.com/spf13/afero v1.1.2
github.com/spf13/afero
github.com/spf13/afero/mem
# github.com/spf13/cast v1.3.0
github.com/spf13/cast
# github.com/spf13/cobra v0.0.5
github.com/spf13/cobra
# github.com/spf13/jwalterweatherman v1.0.0
github.com/spf13/jwalterweatherman
# github.com/spf13/pflag v1.0.5
github.com/spf13/pflag
# github.com/spf13/viper v1.4.0
github.com/spf13/viper
# golang.org/x/sys v0.0.0-20190924154521-2837fb4f24fe
golang.org/x/sys/unix
# golang.org/x/text v0.3.0
golang.org/x/text/transform
golang.org/x/text/unicode/norm
# gopkg.in/go-playground/validator.v9 v9.29.1
gopkg.in/go-playground/validator.v9
# gopkg.in/yaml.v2 v2.2.2
gopkg.in/yaml.v2
