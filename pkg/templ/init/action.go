package initaction

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/everesio/scaffold/pkg/config"
	"gopkg.in/go-playground/validator.v9"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	ttemplate "text/template"

	"github.com/gobuffalo/packr"
)

type Action struct {
	initConfig *config.InitConfig
	vars       map[string]interface{}
}

func NewAction(initConfig *config.InitConfig) *Action {
	vars := map[string]interface{}{
		"Group":       initConfig.Group,
		"Artifact":    initConfig.Artifact,
		"Name":        initConfig.Name,
		"Description": initConfig.Description,
		"PackageName": initConfig.PackageName,
		"PackageFile": strings.ReplaceAll(initConfig.PackageName, ".", "/"),
	}

	return &Action{
		initConfig: initConfig,
		vars:       vars,
	}
}

func (s *Action) Run() error {

	data, err := s.templateBoxedFile("metadata/init.templ")
	if err != nil {
		return err
	}
	var templates []Template
	err = json.Unmarshal([]byte(data), &templates)
	if err != nil {
		return err
	}
	validate := validator.New()
	err = validate.Var(templates, "required,min=1,dive")
	if err != nil {
		return err
	}

	m := Metadata{
		BaseDir:   fmt.Sprintf("%s/%s", s.initConfig.BaseDir, s.initConfig.Artifact),
		Templates: templates,
	}

	for _, entry := range m.Templates {
		filename := fmt.Sprintf("%s/%s", m.BaseDir, entry.Target)
		fmt.Println("Create file", filename)
		data, err := s.templateBoxedFile(entry.Source)
		if err != nil {
			return err
		}

		dirName := filepath.Dir(filename)
		err = os.MkdirAll(dirName, os.ModePerm)
		if err != nil {
			return err
		}
		err = ioutil.WriteFile(filename, []byte(data), 0660)
		if err != nil {
			return err
		}
		//fmt.Println(data)
		_ = data
	}
	return nil
}

func (s *Action) templateBoxedFile(file string) (string, error) {
	box := packr.NewBox("../../../templates")
	data, err := box.FindString(file)
	var tpl bytes.Buffer
	t := ttemplate.Must(ttemplate.New(file).Parse(data))
	err = t.Execute(&tpl, s.vars)
	if err != nil {
		return "", err
	}
	return tpl.String(), nil
}

// unused
func (s *Action) _templateFile(file string) (string, error) {
	b, err := ioutil.ReadFile(file) // just pass the file name
	if err != nil {
		return "", err
	}

	var tpl bytes.Buffer
	t := ttemplate.Must(ttemplate.New(file).Parse(string(b)))
	err = t.Execute(&tpl, s.vars)
	if err != nil {
		return "", err
	}
	return tpl.String(), nil
}

type Metadata struct {
	BaseDir   string
	Templates []Template
}

type Template struct {
	Source string `json:"source" validate:"required"`
	Target string `json:"target" validate:"required"`
}
