package config

import (
	"fmt"
	"github.com/iancoleman/strcase"
	"github.com/pkg/errors"
	"regexp"
	"strings"
)

type InitConfig struct {
	BaseDir string

	Group    string
	Artifact string

	Name        string
	Description string
	PackageName string
}

const (
	nameRegex = `^[a-z]+(.[a-zA-Z_][a-zA-Z0-9_]*)*$`
)

var (
	regexArtifact = regexp.MustCompile(nameRegex)
	regexGroup    = regexp.MustCompile(nameRegex)
	regexPkg      = regexp.MustCompile(nameRegex)
)

func NewInitConfig() *InitConfig {
	return &InitConfig{}
}

func (s *InitConfig) SetDefaults() {
	if s.Name == "" {
		s.Name = s.Artifact
	}
	s.Name = toName(s.Name)
	if s.Description == "" {
		s.Description = "Demo project for Spring Boot"
	}
	if s.PackageName == "" {
		s.PackageName = fmt.Sprintf("%s.%s", s.Group, s.Artifact)
	}
}

func (s *InitConfig) Validate() error {
	if ok := regexGroup.MatchString(s.Group); !ok {
		return errors.Errorf("Group parameter must match %v", regexGroup)
	}
	if ok := regexArtifact.MatchString(s.Artifact); !ok {
		return errors.Errorf("Artifact parameter must match %v", regexGroup)
	}
	if ok := regexPkg.MatchString(s.PackageName); !ok {
		return errors.Errorf("PackageName parameter must match %v", regexPkg)
	}
	return nil
}

func toName(name string) string {
	i := strings.LastIndex(name, ".")
	if i != -1 {
		name = name[i+1:]
	}
	return strcase.ToCamel(name)
}
